<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Request;

use Illuminate\Validation\Validator;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

use App\models\Customer as Customer;

class CustomerController extends Controller
{
    /**
     * Function to show the all area of interest in grid
     * 
     * @return void
     * 
     */
    public function index ()
    {
        $customer = DB::table('customer')->paginate(2);

        return view('customer.index', compact('customer'));
    }
    
    /**
     * Function to display the create page for area of interest
     * 
     * @return void
     * 
     */
    public function create()
    {
        $customer = new customer;
        
        $area_of_interest_fields = DB::table('area_of_interest')->select('interest_title','id')->get();
        $area_of_interest = static::extract($area_of_interest_fields, 'id', 'interest_title');

        return view('customer.create', compact('customer', 'area_of_interest'));
    }

    /**
     * Function to store a new area of interest
     * 
     * @param \Illuminate\Http\Request $request - Request object to fetch the posted values
     * 
     * @return void
     * 
     */
    public function store(Request $request)
    {
        $customer = new Customer;
        $input = $customer->formatDateFields($request->input());
        
        $customer->fill($input['customer']);
        
        if ($customer->save()) {
            $customer->createSubMasters($input, $customer->id);
            return redirect('customer/view/'.$customer->id);
        }

        /*$email = $input['email'];
        $mobile_no = $input['mobile'];
        $dob = date('Y-m-d', strtotime($input['dob']));
             
        $count = max(array(count($email), count($mobile_no)));
        $contact = [];
        for($i=0; $i<$count; $i++) {
            $contact[$i]['email'] = isset($email[$i])?$email[$i]:null;
            $contact[$i]['mobile_no'] = isset($mobile_no[$i])?$mobile_no[$i]:null;
        }

        $validator = \Validator::make($input,[
            'first_name' => 'required|string|max:40',
            'last_name' => 'required|string|max:40',
            'contact' => $contact,

            'duration_from_hsc' => 'required|date',
            'duration_from_ug' => 'required|date',
            'duration_from_pg' => 'required|date',
            'duration_to_hsc' => 'required|date',
            'duration_to_ug' => 'required|date',
            'duration_to_pg' => 'required|date',
            'percentage_hsc' =>  'required|integer|min:0|max:100',
            'percentage_ug'  =>  'required|integer|min:0|max:100',
            'percentage_pg'  =>  'required|integer|min:0|max:100',
            'course_name_hsc' => 'required|string|max:40',
            'course_name_ug' => 'required|string|max:40',
            'course_name_pg' => 'required|string|max:40',
            'institution_hsc' => 'required|string|max:40',
            'institution_ug' => 'required|string|max:40',
            'institution_pg' => 'required|string|max:40',
        ], array());
        
        $validator->each('contact', [
            'email' => 'null|string|max:60',
            'mobile_no' => 'required|string|max:14',
        ]);*/
        //return redirect('customer');
    }
    
    /**
     * 
     * Static function to extract ids and specific field, especially used for creating drop down data.
     * 
     * @param Array $array   - An array object with id and some fields.
     * 
     * @return array $output - Output array with id as key and specified field as value.
     * 
     */
    public static function extract ($array, $id, $value)
    {
        $output = array();
        
        foreach($array as $val)
        {
            $output[$val->id] = $val->$value;
        }

        return $output;
    }
    /**
     * 
     */
    public function view ($id)
    {
        $customer = \App\models\Customer::find($id);
        $blood_group_collection = [
        'op' => 'O+ve', 'on' => 'O-ve', 'bp' => 'B+ve',
        'bn' => 'B-ve', 'ap' => 'A+ve', 'an' => 'A-ve',
        'abp' => 'AB+ve', 'abn' => 'AB-ve'];
        
        return view('customer.view', compact('id', 'customer', 'blood_group_collection'));
    }
    /**
     * Function to display the update page for customer
     * 
     * @return void
     * 
     */
    public function edit($id)
    {
        
        $customer = Customer::find($id);

        return view('customer.edit', compact('customer', 'id'));
    }
    /**
     * Function to update the customer date
     * 
     * @param integer $id
     * 
     * @return void
     * 
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);//Query builder instance
        $customer_obj = new Customer;// Model's general object instance
        $input = $customer_obj->formatDateFields($request->input());

        $customer->fill($input['customer']);
        if ($customer->save()) {
            $customer->updateSubMasters($input, $customer->id);
            return redirect('customer/view/'.$customer->id);
        }
    }
    /**
     * Function to delete a single record detail
     * 
     * @param integere $id - id of the record to be deleted
     * 
     * @return void
     */
    public function delete ($id)
    {
        DB::table('mobile')->where('customer_id', '=', $id)->delete();
        DB::table('email')->where('customer_id', '=', $id)->delete();
        
        DB::table('education_hsc')->where('customer_id', '=', $id)->delete();
        DB::table('education_ug')->where('customer_id', '=', $id)->delete();
        DB::table('education_pg')->where('customer_id', '=', $id)->delete();
        
        DB::table('customer')->where('id', '=', $id)->delete();
        
        return redirect('customer');
    }    
}

//$input = $request->input();
//        
//        $customer = new Customer;
//        $input = $customer->formatDateFields($input);
//        
//        $customer->fill($input['customer']);
//        
//        if ($customer->save()) {
//            $customer->createSubMasters($input, $customer->id);
//            return redirect('customer/view/'.$customer->id);
//        }