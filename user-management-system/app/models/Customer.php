<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

use App\models\Mobile as Mobile;

use App\models\Email as Email;

use App\models\EducationHsc as EducationHsc;

class Customer extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer';
    protected $fillable = array('first_name', 'last_name', 'dob', 'gender', 'blood_group');
    /**
     * Get the email record associated with the user.
     */
    public function email() {
        return $this->hasMany('App\models\Email');
    }

    /**
     * Get the mobile record associated with the user.
     */
    public function mobile() {
        return $this->hasMany('App\models\Mobile');
    }

    /**
     * Get the educationHsc record associated with the user.
     */
    public function educationHsc() {
        return $this->hasOne('App\models\EducationHsc');
    }

    /**
     * Get the educationUg record associated with the user.
     */
    public function educationUg() {
        return $this->hasOne('App\models\EducationUg');
    }

    /**
     * Get the educationPg record associated with the user.
     */
    public function educationPg() {
        return $this->hasOne('App\models\EducationPg');
    }

    /**
     * After a customer record has been created this method is used to 
     * create the sub-masters of the customer
     * 
     * @param array $input         - $input array with all fields of the customer
     * @param integer $customer_id - customer id
     * 
     * @return void
     */
    public function createSubMasters($input, $customer_id) {

        $this->createMobile($input['mobile'], $customer_id);
        $this->createEmail($input['email'], $customer_id);
        
        $this->createEducation(new \App\models\EducationHsc, $customer_id, $input['education_hsc']);
        $this->createEducation(new \App\models\EducationUg, $customer_id, $input['education_ug']);
        $this->createEducation(new \App\models\EducationPg, $customer_id, $input['education_pg']);

    }
    /**
     * After a customer record has been updated this method is used to 
     * update the related sub-masters of the customer
     * 
     * @param array $input         - $input array with all fields of the customer
     * @param integer $customer_id - customer id
     * 
     * @return void
     */
    public function updateSubMasters($input, $customer_id) {

        $this->updateMobile($input['mobile'], $customer_id);
        $this->updateEmail($input['email'], $customer_id);

        $this->updateEducation(get_class(new \App\models\EducationHsc), $customer_id, $input['education_hsc']);
        $this->updateEducation(get_class(new \App\models\EducationUg), $customer_id, $input['education_ug']);
        $this->updateEducation(get_class(new \App\models\EducationPg), $customer_id, $input['education_pg']);
    }
    /**
     * Function create mobile details of the created customer
     * 
     * @param array $input         - $input array with all fields of the customer
     * @param integer $customer_id - customer id
     * 
     * @return void
     */
    public function createMobile($mobile_no, $customer_id)
    {
        /* @var $mobile_no array */
        $mobile_no = array_unique($mobile_no);//Remove duplicate entries
        
        foreach ($mobile_no as $mobile) {
            $mobile_model = new \App\models\Mobile;
            $mobile_model->mobile_no = $mobile;
            $mobile_model->customer_id = $customer_id;
            $mobile_model->save();
        }
    }
    /**
     * Function to update mobile details of the updated customer
     * 
     * @param array $input         - $input array with all fields of the customer
     * @param integer $customer_id - customer id
     * 
     * @return void
     */
    public function updateMobile($mobile_no, $customer_id)
    {
        Mobile::where('customer_id', $customer_id)->delete();

        foreach (array_unique($mobile_no) as $mobile) {
            $mobile_model = new \App\models\Mobile;
            $mobile_model->mobile_no = $mobile;
            $mobile_model->customer_id = $customer_id;
            $mobile_model->save();
        }
    } 
    
    /**
     * Function create email details of the created customer
     * 
     * @param array $input         - $input array with all fields of the customer
     * @param integer $customer_id - customer id
     * 
     * @return void
     */        
    public function createEmail($email, $customer_id)
    {
        
        foreach (array_unique($email) as $email_id) {
            $email_model = new \App\models\Email;
            $email_model->email_id = $email_id;
            $email_model->customer_id = $customer_id;
            $email_model->save();
        }
    }
    /**
     * Function create email details of the created customer
     * 
     * @param array $input         - $input array with all fields of the customer
     * @param integer $customer_id - customer id
     * 
     * @return void
     */        
    public function updateEmail($email, $customer_id)
    {
        Email::where('customer_id', $customer_id)->delete();
        /* @var $email array */
        $email = array_unique($email);//Remove duplicate entries
        
        foreach ($email as $email_id) {
            $email_model = new \App\models\Email;
            $email_model->email_id = $email_id;
            $email_model->customer_id = $customer_id;
            $email_model->save();
        }
    }    
    /**
     * Function create HSC education details of the created customer
     * 
     * @param array $input         - $input array with all fields of the customer
     * @param integer $customer_id - customer id
     * 
     * @return void
     */
    public function createEducation($education, $customer_id, $input)
    {
        $education->customer_id = $customer_id;
        $education->fill($input);
        $education->save();
    }
    /**
     * Function update education details of the created customer
     * 
     * @param string  $education   - class name of any of the 3 classes (EducationHsc, EducationUg, EducationPg)
     * @param integer $customer_id - customer id
     * @param array $input         - $input array with all fields of the customer
     * 
     * @return void
     */
    public function updateEducation($education, $customer_id, $input)
    {
        $education::updateOrCreate(['customer_id' => $customer_id], $input);
    }    
    /**
     * Format all the date fields in customer form input
     * 
     * @param array $input  - $input array with all customer related fields 
     * @return array $input
     */
    public function formatDateFields($input)
    {
        $input['customer']['dob'] = date('Y-m-d', strtotime($input['customer']['dob']));

        $input['education_hsc']['duration_from'] = date('Y', strtotime($input['education_hsc']['duration_from']));
        $input['education_hsc']['duration_to'] = date('Y', strtotime($input['education_hsc']['duration_to']));
        
        $input['education_ug']['duration_from'] = date('Y', strtotime($input['education_ug']['duration_from']));
        $input['education_ug']['duration_to'] = date('Y', strtotime($input['education_ug']['duration_to']));

        $input['education_pg']['duration_from'] = date('Y', strtotime($input['education_pg']['duration_from']));
        $input['education_pg']['duration_to'] = date('Y', strtotime($input['education_pg']['duration_to']));
        return $input;
    }
}

