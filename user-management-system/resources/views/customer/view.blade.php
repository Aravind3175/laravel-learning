@extends('layout')

@section('content')


<?php
    
?>

<h1>View Customer</h1>

<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Dob</th>
            <th>Gender</th>
            <th>Blood Group</th>
            <th>Higher Secondary</th>
            <th>Under Graduation</th>
            <th>Post Graduation</th>
        </tr>
        <tr>
            <td>{{ $customer->first_name." ".$customer->last_name }}</td>
            <td>{{ date("m/d/Y", strtotime($customer->dob)) }}</td>
            <td>{{ ($customer->gender == 'm')?'Male':'Female' }}</td>
            <td>{{ $blood_group_collection[$customer->blood_group] }}</td>
            <td>{{ $customer->EducationHsc->course_name }}</td>
            <td>{{ $customer->EducationUg->course_name }}</td>
            <td>{{ $customer->EducationPg->course_name }}</td>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>


@endsection

@section('sidenav')

<a href="{{ url('/customer/create/') }}" class="list-group-item">Create</a>

<a href="{{ url('/customer/edit/'.$customer->id) }}" class="list-group-item">Update</a>

<a href="{{ url('/customer/delete/') }}" class="list-group-item">Delete</a>

<a href="{{ url('/customer') }}" class="list-group-item">List</a>

@endsection