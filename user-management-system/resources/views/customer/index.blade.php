@extends('layout')

@section('content')
<h1>List Customer</h1>

@if ($errors->any())

    @foreach($errors->all(':message') as $err)
        <div class="alert-danger" style="padding: 5px 5px 5px 15px;">{{$err}}</div>
    @endforeach

@endif

<table class="table table-striped">
    <thead>
        <tr>
            <th>Interest Title</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
@foreach ($customer as $val)
        <tr>
            <td>{{ $val->first_name.$val->last_name }}</td>
            <td>
                <a href="{{ url('/customer/view/'.$val->id) }}" >
                    View
                </a>
                <a href="{{ url('/customer/edit/'.$val->id) }}" >
                    Edit
                </a>
                <a href="{{ url('/customer/delete/'.$val->id) }}" >
                    Delete
                </a>
            </td>
        </tr>
@endforeach
    </tbody>
</table>
@endsection
@section('pagination')
    {!! $customer->render() !!}
@endsection

@section('sidenav')

<a href="{{ url('/customer/create/') }}" class="list-group-item">Create</a>

@endsection
