<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/******************* Area of interest Master **********************************/

Route::get('area-of-interest', 'AreaOfInterestController@index');

Route::get('area-of-interest/create', 'AreaOfInterestController@create');

Route::get('area-of-interest/edit/{id}', 'AreaOfInterestController@edit');

Route::post('area-of-interest/update/{id}', 'AreaOfInterestController@update');

Route::get('area-of-interest/view/{id}', 'AreaOfInterestController@view');

Route::get('area-of-interest/delete/{id}', 'AreaOfInterestController@delete');

Route::post('area-of-interest/store', 'AreaOfInterestController@store');

/******************* Area of interest Master **********************************/

/*************************** Customer Master **********************************/

Route::get('customer', 'CustomerController@index');

Route::get('customer/create', 'CustomerController@create');

Route::get('customer/edit/{id}', 'CustomerController@edit');

Route::post('customer/update/{id}', 'CustomerController@update');

Route::get('customer/view/{id}', 'CustomerController@view');

Route::get('customer/delete/{id}', 'CustomerController@delete');

Route::post('customer/store', 'CustomerController@store');

/*************************** Customer Master **********************************/