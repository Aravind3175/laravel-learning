<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CustomerValidator extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'first_name' => 'bail|required|string|max:40',
		'last_name' => 'bail|required|string|max:40',
                'email' => 'bail|required|string|max:60',
		'mobile_no' => 'required|string|max:14',

		'duration_from_hsc' => 'required|date',
                'duration_from_ug' => 'required|date',
                'duration_from_pg' => 'required|date',
		'duration_to_hsc' => 'required|date',
                'duration_to_ug' => 'required|date',
                'duration_to_pg' => 'required|date',
		'percentage_hsc' =>  'required|integer|min:0|max:100',
                'percentage_ug'  =>  'required|integer|min:0|max:100',
                'percentage_pg'  =>  'required|integer|min:0|max:100',
		'course_name_hsc' => 'required|string|max:40',
                'course_name_ug' => 'required|string|max:40',
                'course_name_pg' => 'required|string|max:40',
		'institution_hsc' => 'required|string|max:40',
                'institution_ug' => 'required|string|max:40',
                'institution_pg' => 'required|string|max:40',
        ];
    }
    
    public function messages()
    {
        return [
            'first_name.required' => 'First name field is Must one'
        ];
    }
    
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $validator->errors()->add('first_name', 'Something is wrong with this field!');
        });
    }
}
