<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Mobile extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mobile';
    /**
     * Get the customer that owns the phone.
     */
    public function customer()
    {
        return $this->belongsTo('App\models\Customer');
    }    
}