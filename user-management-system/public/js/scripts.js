  $(function() {
    $( ".datepicker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '1960:2050'
    });
  });
var clones = {
    email_clone_count:0,
    mobile_clone_count:0,
}

$("body").on('click', ".add-email, .add-mobile", function() {

  if($(this).hasClass('add-email')) {
    email_box_clone = $(".email-box:last").clone();
    
    clones.email_clone_count++;
    email_box_clone.find('.form-control')
                .attr('id', 'email_'+clones.email_clone_count)
                .val('');
    $(".email-box:last").append(email_box_clone);
    console.log($(".email-box").length);

    if($(".email-box").length < 3)
        $(".email-box:last").append("<i class='fa fa-trash-o alert-danger'></i>");
    }

  if($(this).hasClass('add-mobile')) {
    mobile_box_clone = $(".mobile-box:last").clone();
    
    clones.mobile_clone_count++;
    mobile_box_clone.find('.form-control')
                .attr('id', 'mobile_'+clones.email_clone_count)
                .val('');
        
    $(".mobile-box:last").append(mobile_box_clone);
    if($(".mobile-box").length < 3)
        $(".mobile-box:last").append("<i class='fa fa-trash-o alert-danger'></i>");
  }

});

$("body").on('click', ".email-delete, .mobile-delete", function() {

    if($(this).hasClass('mobile-delete')) {
        $(this).parent().remove();
    }

    if($(this).hasClass('email-delete')) {
        $(this).parent().remove();
    }
});
