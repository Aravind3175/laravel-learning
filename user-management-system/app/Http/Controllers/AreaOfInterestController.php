<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Request;

use Illuminate\Support\Facades\DB;

use App\models\AreaOfInterest as AreaOfInterest;

class AreaOfInterestController extends Controller
{
    /**
     * Function to show the all area of interest in grid
     * 
     * @return void
     * 
     */
    public function index ()
    {
        $area_of_interest = DB::table('area_of_interest')->get();

        return view('area-of-interest.index', compact('area_of_interest'));
    }
    
    /**
     * Function to display the create page for area of interest
     * 
     * @return void
     * 
     */
    public function create ()
    {
        return view('area-of-interest.create');
    }
    /**
     * Function to store a new area of interest
     * 
     * @param \Illuminate\Http\Request $request - Request object to fetch the posted values
     * 
     * @return void
     * 
     */
    public function store (Request $request)
    {

        $this->validate($request, ['interest_title' => 'required|string|max:40']);
        
        $area_of_interest = new AreaOfInterest;
        
        $area_of_interest->interest_title = $request->interest_title;
        
        $area_of_interest->save();
        
        return redirect('area-of-interest');
    }
    /**
     * Function to display the edit page for a specific area of interest
     * 
     * @param int $id id of the record to be edited
     * 
     * @return void
     * 
     */
    public function edit ($id)
    {
        $area_of_interest = AreaOfInterest::find($id);

        $interest_title = $area_of_interest->interest_title;
        
        return view('area-of-interest.edit', compact('interest_title', 'id'));
    }
    /**
     * Function to update the existing values of a master
     * 
     * @param \Illuminate\Http\Request $request - Request object to fetch the posted values
     * @param int                      $id      - id of the record to be updated
     * 
     * @return void
     * 
     */
    public function update (Request $request, $id)
    {
        $area_of_interest = AreaOfInterest::find($id);

        $input = $request->input();

        $area_of_interest->interest_title = $input['interest_title'];
        
        $this->validate($request, ['interest_title' => 'required|string|max:40']);
        
        if ($area_of_interest->save()) {
            return redirect('area-of-interest/view/'.$id);
        }
    }
    /**
     * Function to display the single record details
     * 
     * @param $id - id of the record to be viewed
     * 
     * @return void
     * 
     */
    public function view ($id)
    {
        $area_of_interest = AreaOfInterest::find($id);
        
        return view('area-of-interest.view', compact('id', 'area_of_interest'));
    }
    /**
     * Function to delete a single record detail
     * 
     * @param integere $id - id of the record to be deleted
     * 
     * @return void
     */
    public function delete ($id)
    {
        DB::table('area_of_interest')->where('id', '=', $id)->delete();
        
        return redirect('area-of-interest');
    }
}