<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class AreaOfInterest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'area_of_interest';
}