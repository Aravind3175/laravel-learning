@extends('layout')

@section('title')
Edit Area Of Interest
@endsection

@section('content')
<h1>Edit Area of Interest</h1>
<form class="form" role="form" method="POST" action="{{ url('area-of-interest/update/'.$id) }}">
    {{ csrf_field() }}
    
    @if ($errors->any())
        <div class="alert-danger">{{ implode('', $errors->all(':message')) }}</div>
    @endif
    
    <div class="form-group">
        {{ Form::label('interest_title', 'Interest Title') }}
        <input type="text" name="interest_title" class="form-control" id="interest" value= "{{$interest_title}}" >
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>
@endsection

@section('sidenav')

<a href="{{ url('/area-of-interest/create/') }}" class="list-group-item">Create</a>

<a href="{{ url('/area-of-interest/delete/') }}" class="list-group-item">Delete</a>

<a href="{{ url('/area-of-interest/view/'.$id) }}" class="list-group-item">View</a>

<a href="{{ url('/area-of-interest/') }}" class="list-group-item">List</a>

@endsection