# README #

### What is this repository for? ###

* This repository is to update my laravel learnings
* It has two projects blog and user mangagement system where blog is the place where I practice my learnings
* and user management is my practice project.
* Version 1.0

### How do I get set up? ###

* Summary of set up
	Here I have included two projects. I am using the blog project to describe the project set up. You need composer to be setup in your system globally or locally. clone this repo and cd laravel installer/blog. Then

	Run command : composer install

* Dependencies
	/laravel learning/blog/composer.json

* Database configuration
	Update the database name and database username in project root folder/.env file and run the command.	
	“php artisan migrate”
	
	It actually checks the files under the migration folder and if there is any class without tables those table will be created after this command.

* How to run tests
