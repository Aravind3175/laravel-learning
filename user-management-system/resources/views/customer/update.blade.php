@extends('layout')

@section('title')
Update Customer
@endsection

@section('content')
<!--{{ Form::model($customer, ['action'=> ['CustomerController@store']]) }}-->
<!--{!! Form::open(['action'=> 'CustomerController@store']) !!}-->
<?= Form::open(['action'=> 'CustomerController@update']); ?>
<?php




?>
@if ($errors->any())

    @foreach($errors->all(':message') as $err)
        <div class="alert-danger" style="padding: 5px 5px 5px 15px;">{{$err}}</div>
    @endforeach

@endif

{{ csrf_field() }}
<!-- First Name -->
<div class="form-group">
    <?= Form::label('first_name', 'First Name') ?>
    <?= Form::text('customer[first_name]', $customer->first_name, ['class' => 'form-control']) ?>
</div>

<!-- Last Name -->
<div class="form-group">
    <?= Form::label('last_name', 'Last Name') ?>
    <?= Form::text('customer[last_name]', $customer->last_name, ['class' => 'form-control']) ?>
</div>

<!-- Blood Group -->
<div class="form-group">
    <?= Form::label('Blood Group', 'Select Blood Group', ['class' => 'control-label'] )  ?>
    <?= Form::select('customer[blood_group]', ['op' => 'O+ve', 'on' => 'O-ve', 'bp' => 'B+ve',
        'bn' => 'B-ve', 'ap' => 'A+ve', 'an' => 'A-ve', 'abp' => 'AB+ve', 'abn' => 'AB-ve'], 
        $customer->blood_group, ['class' => 'form-control' ]) ?>
</div>

<!-- Gender -->
<div class="form-group">
    <div class="radio">
        <?= Form::label('gender', 'Gender', ['class' => 'control-label']) ?>&nbsp;&nbsp;&nbsp;
        
        <?= Form::radio('male', 'm', true, ['id' => 'male', 'class' => 'radio-inline', 'name'=>"customer[gender]"]) ?>
        <?= Form::label('male', 'Male', ['class' => 'radio-inline']) ?>

        <?= Form::radio('male', 'f', false, ['id' => 'female', 'class' => 'radio-inline', 'name'=>"customer[gender]"]) ?>
        <?= Form::label('male', 'FeMale', ['class' => 'radio-inline']) ?>
    </div>
</div>

<!-- Email -->
<div class="form-group email-container">
    <?php
    foreach( $customer->email as $email) { ?>
        <div class="email-box" style="padding-bottom: 10px;">
            <?= Form::label('email', 'Email', ['class' => 'control-label email-label']);?>
            <?= Form::text('customer[email_id]', $email->email_id, ['class' => 'form-control email-field', 'name'=>'email[]']);  ?>
        </div>
    <?php
    }
    ?>
    
    <input type="button" class="btn btn-primary add-email" value="Add More" />
</div>

<!-- Mobile -->
<div class="form-group">
        <?php
    foreach( $customer->mobile as $mobile) { ?>
        <div class="mobile-box" style="padding-bottom: 10px;">
            <?= Form::label('mobile', 'Mobile', ['class' => 'control-label']);?>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?= Form::text('customer[mobile]', $mobile->mobile_no, ['class' => 'form-control', 'name'=>'mobile[]']);  ?>
        </div>
    <?php
    }
    ?>

    <input type="button" class="btn btn-primary add-mobile" value="Add More" />
</div>

<!-- Dob -->
<div class="form-group">
    <?= Form::label('dob', 'Date Of Birth'); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?= Form::text('customer[dob]', date("m/d/Y", strtotime($customer->dob)), ['class' => 'datepicker']) ?>
</div>

<!-- Blood Group --><?php /*
<div class="form-group">
    <?= Form::label('area_of_interest', 'Area Of Interest', ['class' => 'control-label'] )  ?>
    <?= Form::select('area_of_interest', $area_of_interest, '', ['class' => 'form-control']) ?>
</div> */ ?>

<fieldset class="col-md-12">
    <h3>Education Details</h3>
    <table class="table table-striped">
        <thead>
            <tr>
                <th></th>
                <th>From</th>
                <th>To</th>
                <th>Percentage</th>
                <th>Course Name</th>
                <th>Institution</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Higher Secondary</td>
                <td><?= Form::text('education_hsc[duration_from]', $customer->EducationHsc->duration_from, ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_hsc[duration_to]', $customer->EducationHsc->duration_to, ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_hsc[percentage]', $customer->EducationHsc->percentage, ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_hsc[course_name]', $customer->EducationHsc->course_name, ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_hsc[institution]', $customer->EducationHsc->institution, ['class' => 'form_control']);  ?></td>
            </tr>
            <tr>
                <td>Under Graduation</td>
                <td><?= Form::text('education_ug[duration_from]', $customer->EducationUg->duration_from, ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_ug[duration_to]', $customer->EducationUg->duration_to, ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_ug[percentage]', $customer->EducationUg->percentage, ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_ug[course_name]', $customer->EducationUg->course_name, ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_ug[institution]', $customer->EducationUg->institution, ['class' => 'form_control']);  ?></td>
            </tr>
            <tr>
                <td>Post Graduation</td>
                <td><?= Form::text('education_pg[duration_from]', $customer->EducationPg->duration_from, ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_pg[duration_to]', $customer->EducationPg->duration_to, ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_pg[percentage]', $customer->EducationPg->percentage, ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_pg[course_name]', $customer->EducationPg->course_name, ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_pg[institution]', $customer->EducationPg->institution, ['class' => 'form_control']);  ?></td>
            </tr>
        </tbody>
    </table>
</fieldset>

<?= Form::submit('Update Customer', ['class' => 'btn btn-primary']); ?>

<?= Form::close(); ?>

@endsection

@section('sidenav')

<a href="{{ url('/customer/') }}" class="list-group-item">List</a>

@endsection
