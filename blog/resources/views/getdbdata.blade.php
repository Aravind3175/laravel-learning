<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                    <ul>
                    @foreach ( $tasks as $task)
                        <li>{{$task->title}}</li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>
