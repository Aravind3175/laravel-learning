@extends('layout')

@section('title')
Create Area Of Interest
@endsection

@section('content')
<h1>Create Area of Interest</h1>
<form class="form" role="form" method="POST" action="{{ url('/area-of-interest/store') }}">
    
    @if ($errors->any())
        <div class="alert-danger">{{ implode('', $errors->all(':message')) }}</div>
    @endif
    
    {{ csrf_field() }}
    <div class="form-group">
        <label for="interest">Interest Title:</label>
        <input type="text" name="interest_title" class="form-control" id="interest">
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>
@endsection

@section('sidenav')

<a href="{{ url('/area-of-interest/') }}" class="list-group-item">List</a>

@endsection