@extends('layout')

@section('content')
<h1>List Area of Interest</h1>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Interest Title</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
@foreach ($area_of_interest as $val)
    
        <tr>
            <td>{{ $val->interest_title }}</td>
            <td>
                <a href="{{ url('/area-of-interest/view/'.$val->id) }}" >
                    View
                </a>
                <a href="{{ url('/area-of-interest/edit/'.$val->id) }}" >
                    Edit
                </a>
                <a href="{{ url('/area-of-interest/delete/'.$val->id) }}" >
                    Delete
                </a>
            </td>
        </tr>
@endforeach
    </tbody>
</table>
@endsection

@section('sidenav')

<a href="{{ url('/area-of-interest/create/') }}" class="list-group-item">Create</a>

@endsection