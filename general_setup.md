# GENERAL SETUP FOR LARAVEL #

### How do I get set up? ###

* Summary of set up
	Here I have included two projects. I am using the blog project to describe the set up. You need composer to be setup in your system globally or locally
	Run command : composer create-project --prefer-dist laravel/laravel blog 5.2.45
	give write permission to storage and bootstrap/cache folders.

	chmod 777 -R /laravel learning/blog/bootstrap/cache
	chmod 777 -R /laravel learning/blog/storage

	composer install

* Dependencies
	/laravel learning/blog/composer.json

* Database configuration
	Update the database name and database username in project root folder/.env file and run the command.	
	“php artisan migrate” 
	
	It actually checks the files under the migration folder and if there is any class without tables those table will be created after this command.
	
* How to run tests
* Deployment instructions
