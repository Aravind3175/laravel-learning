<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/pass-variables', function () {

	$page = "Laravel variable rendering";
	$author = "Aravind Raj";

    //return view('pass-variables', compact('page','author'));
    return view('pass-variables')->with('author', 'Aravind Raj');
});

Route::get('/getdbdata', function () {

	$page = "Get db data";
	$tasks = DB::table('tasks')->get();

    return view('getdbdata', compact('page', 'tasks'));
});

/*Route::get('/getdbdata/{id}', function ($id) {

	$page = "Get db data";
	$tasks = DB::table('tasks')->find($id);

    return view('getdbdata', compact('page', 'tasks'));
});*/
