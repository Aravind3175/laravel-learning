@extends('layout')

@section('content')
@foreach ($area_of_interest as $val)
    {{ $val->interest_title }}
@endforeach
@endsection

@section('sidenav')

<a href="{{ url('/area-of-interest/create/') }}" class="list-group-item">Create</a>

<a href="{{ url('/area-of-interest/edit/'.$area_of_interest->id) }}" class="list-group-item">Update</a>

<a href="{{ url('/area-of-interest/delete/') }}" class="list-group-item">Delete</a>

<a href="{{ url('/area-of-interest/') }}" class="list-group-item">List</a>

@endsection