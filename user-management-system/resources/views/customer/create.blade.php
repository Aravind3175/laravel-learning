@extends('layout')

@section('title')
Create Customer
@endsection

@section('content')

<h1>Create Customer</h1>

<?= Form::open(['action'=> 'CustomerController@store']); ?>
<?php




?>
@if ($errors->any())

    @foreach($errors->all(':message') as $err)
        <div class="alert-danger" style="padding: 5px 5px 5px 15px;">{{$err}}</div>
    @endforeach

@endif

{{ csrf_field() }}
<!-- First Name -->
<div class="form-group">
    <?= Form::label('first_name', 'First Name') ?>
    <?= Form::text('customer[first_name]', null, ['class' => 'form-control']) ?>
</div>

<!-- Last Name -->
<div class="form-group">
    <?= Form::label('last_name', 'Last Name') ?>
    <?= Form::text('customer[last_name]', null, ['class' => 'form-control']) ?>
</div>

<!-- Blood Group -->
<div class="form-group">
    <?= Form::label('Blood Group', 'Select Blood Group', ['class' => 'control-label'] )  ?>
    <?= Form::select('customer[blood_group]', ['op' => 'O+ve', 'on' => 'O-ve', 'bp' => 'B+ve',
        'bn' => 'B-ve', 'ap' => 'A+ve', 'an' => 'A-ve', 'abp' => 'AB+ve', 'abn' => 'AB-ve'], 
        '', ['class' => 'form-control' ]) ?>
</div>

<!-- Gender -->
<div class="form-group">
    <div class="radio">
        <?= Form::label('gender', 'Gender', ['class' => 'control-label']) ?>&nbsp;&nbsp;&nbsp;
        
        <?= Form::radio('male', 'm', true, ['id' => 'male', 'class' => 'radio-inline', 'name'=>"customer[gender]"]) ?>
        <?= Form::label('male', 'Male', ['class' => 'radio-inline']) ?>

        <?= Form::radio('male', 'f', false, ['id' => 'female', 'class' => 'radio-inline', 'name'=>"customer[gender]"]) ?>
        <?= Form::label('male', 'FeMale', ['class' => 'radio-inline']) ?>
    </div>
</div>

<!-- Email -->
<div class="form-group email-container">
    <div class="email-box" style="padding-bottom: 10px;">
        <?= Form::label('email', 'Email', ['class' => 'control-label email-label']);?>
        <?= Form::text('customer[email_id]', '', ['class' => 'form-control email-field', 'name'=>'email[]']);  ?>
    </div>
    <input type="button" class="btn btn-primary add-email" value="Add More" />
</div>

<!-- Mobile -->
<div class="form-group">
    <div class="mobile-box" style="padding-bottom: 10px;">
        <?= Form::label('mobile', 'Mobile', ['class' => 'control-label']);?>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <?= Form::text('customer[mobile]', '', ['class' => 'form-control', 'name'=>'mobile[]']);  ?>
    </div>
    <input type="button" class="btn btn-primary add-mobile" value="Add More" />
</div>

<!-- Dob -->
<div class="form-group">
    <?= Form::label('dob', 'Date Of Birth'); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?= Form::text('customer[dob]', null, ['class' => 'datepicker']) ?>
</div>

<!-- Blood Group --><?php /*
<div class="form-group">
    <?= Form::label('area_of_interest', 'Area Of Interest', ['class' => 'control-label'] )  ?>
    <?= Form::select('area_of_interest', $area_of_interest, '', ['class' => 'form-control']) ?>
</div> */ ?>

<fieldset class="col-md-12">
    <h3>Education Details</h3>
    <table class="table table-striped">
        <thead>
            <tr>
                <th></th>
                <th>From</th>
                <th>To</th>
                <th>Percentage</th>
                <th>Course Name</th>
                <th>Institution</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Higher Secondary</td>
                <td><?= Form::text('education_hsc[duration_from]', '', ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_hsc[duration_to]', '', ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_hsc[percentage]', '', ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_hsc[course_name]', '', ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_hsc[institution]', '', ['class' => 'form_control']);  ?></td>
            </tr>
            <tr>
                <td>Under Graduation</td>
                <td><?= Form::text('education_ug[duration_from]', '', ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_ug[duration_to]', '', ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_ug[percentage]', '', ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_ug[course_name]', '', ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_ug[institution]', '', ['class' => 'form_control']);  ?></td>
            </tr>
            <tr>
                <td>Post Graduation</td>
                <td><?= Form::text('education_pg[duration_from]', '', ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_pg[duration_to]', '', ['class' => 'datepicker']);  ?></td>
                <td><?= Form::text('education_pg[percentage]', '', ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_pg[course_name]', '', ['class' => 'form_control']);  ?></td>
                <td><?= Form::text('education_pg[institution]', '', ['class' => 'form_control']);  ?></td>
            </tr>
        </tbody>
    </table>
</fieldset>

<?= Form::submit('Create Customer', ['class' => 'btn btn-primary']); ?>

<?= Form::close(); ?>

@endsection

@section('sidenav')

<a href="{{ url('/customer/') }}" class="list-group-item">List</a>

@endsection