<!DOCTYPE html>
<html lang="en">
    @include('head')
    <body>
        @include('nav')
        <div class="container">
            <div class="row row-offcanvas row-offcanvas-right">
                <div class="col-12 col-md-9">
                    @yield('content')
                    @yield('pagination')
                </div><!--/span-->

                <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
                    <div class="list-group">
			@yield('sidenav')
                    </div>
                </div><!--/span-->
            </div><!--/row-->

            <hr>

            <footer>
                <p>&copy; aravindraj.in</p>
            </footer>

        </div><!--/.container--> 
        @include('footer');
        
    </body>
</html>
