<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class EducationUg extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'education_ug';
    /**
     * Get the customer that owns the phone.
     */
    public function customer()
    {
        return $this->belongsTo('App\models\Customer');
    }
    
    protected $fillable = array('duration_from', 'duration_to', 'percentage', 'course_name', 'institution');
}