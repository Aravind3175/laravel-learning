@extends('layout')

@section('content')
<h1>View Area of Interest</h1>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Interest Title</th>
        </tr>
        <tr>
            <td>{{ $area_of_interest->interest_title }}</td>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>


@endsection

@section('sidenav')

<a href="{{ url('/area-of-interest/create/') }}" class="list-group-item">Create</a>

<a href="{{ url('/area-of-interest/edit/'.$area_of_interest->id) }}" class="list-group-item">Update</a>

<a href="{{ url('/area-of-interest/delete/') }}" class="list-group-item">Delete</a>

<a href="{{ url('/area-of-interest') }}" class="list-group-item">List</a>

@endsection